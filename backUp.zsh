#!/usr/bin/env zsh
set -uo pipefail
## where to log and back up to
USR="klt"
MON=$(date "+%Y-%m")
LOGFILE="${0:r}_${MON}.log"
BACKUPROOT="/media/${USR}/Backup"
BACKUPFOLDER="${BACKUPROOT}/${MON}"
## which folders to back up
MUNIENDA=("/home" "/var/lib/postgresql")

exec 1>> ${LOGFILE} 2>> ${LOGFILE}

function log {
    local msg=$1
    echo $(date "+%Y-%m-%d %H:%M:%S")" $msg"
}

log "START $0: running as $(whoami)"

function backUp {
    local muniendum=$1
    local destinatum=$2
    mkdir -p ${destinatum}
    rsync --archive ${muniendum} ${destinatum}
    du -hs ${muniendum} ${destinatum}/${muniendum##*/}
}

## execution
if [[ -d ${BACKUPROOT} && -w ${BACKUPROOT} ]]; then
    for m in ${MUNIENDA}; do
        if [[ -r ${m} ]]; then
            log "START backup ${m} ${BACKUPFOLDER}"
            backUp ${m} ${BACKUPFOLDER} 1>> ${LOGFILE} 2>> ${LOGFILE}
            log "DONE backup ${m} ${BACKUPFOLDER}"
            log "Remaining space on ${BACKUPROOT}:"
        else
            log "!!! cannot read from ${m}"
        fi
    done
    df -hT ${BACKUPROOT}
else
    log "!!! cannot write to BACKUPROOT: ${BACKUPROOT}"
fi

log "DONE $0"
echo

exit
